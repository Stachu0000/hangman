import java.util.ArrayList;
import java.util.Scanner;

public class GameSetup {
    Scanner scanner = new Scanner(System.in);
    String password = setPassword();
    String emptyPassword = setEmptyPassword();
    String hangman1 = "\r\n\r\n\r\n\r\n\r\n\r\n\r\n//\\\\";
    String hangman2 = "\r\n  |\r\n  |\r\n  |\r\n  |\r\n  |\r\n  |\r\n//\\\\";
    String hangman3 = "   _______________" + hangman2;
    String hangman4 = """
               _______________ \r
              |               |\r
              |               |\r
              |\r
              |\r
              |\r
              |\r
              |\r
            //\\\\""";
    String hangman5 = """
               _______________ \r
              |               |\r
              |               |\r
              |               O\r
              |\r
              |\r
              |\r
              |\r
            //\\\\""";

    String hangman6 = """
               _______________ \r
              |               |\r
              |               |\r
              |               O\r
              |              /|\\\r
              |\r
              |\r
              |\r
            //\\\\""";

    String hangman7 = """
               _______________ \r
              |               |\r
              |               |\r
              |               O\r
              |              /|\\\r
              |              / \\\r
              |\r
              |\r
            //\\\\""";

    ArrayList<String> hangmanList = setHangmanList();

    public ArrayList<String> setHangmanList() {
        ArrayList<String> hangmanList = new ArrayList<>();
        hangmanList.add(hangman1);
        hangmanList.add(hangman2);
        hangmanList.add(hangman3);
        hangmanList.add(hangman4);
        hangmanList.add(hangman5);
        hangmanList.add(hangman6);
        hangmanList.add(hangman7);
        return hangmanList;
    }

    public ArrayList<String> getHangmanList() {
        return hangmanList;
    }

    private String setPassword() {
        System.out.println("Podaj hasło do gry!");
        return scanner.nextLine();
    }

    public String getPassword() {
        return password;
    }

    private String setEmptyPassword() {
        return "_".repeat(password.length());
    }

    public String getEmptyPassword() {
        return emptyPassword;
    }
}
