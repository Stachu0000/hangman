import java.util.ArrayList;
import java.util.Scanner;

public class Game {
    Scanner scanner = new Scanner(System.in);
    GameSetup gameSetup = new GameSetup();
    ArrayList<String> hangManList = gameSetup.getHangmanList();
    String password = gameSetup.getPassword();
    String emptyPassword = gameSetup.getEmptyPassword();

    int counter = 0;

    public void Start() {
        System.out.println("Witaj, hasło do odgadnięcia ma " + password.length() + " liter. Zaczynamy!!!");

        while (true) {

            System.out.println("\r\nPodaj literę do hasła.");
            String letter = scanner.nextLine();

            if (!password.contains(letter)) {

                if (lastHangman()) {
                    break;
                }
                System.out.println(hangManList.get(counter));
                System.out.println("Pudło!!! Spróbuj raz jeszcze.");
                counter++;

            } else {

                int index = password.indexOf(letter);
                StringBuilder sb = new StringBuilder(emptyPassword);

                while (index >= 0) {
                    sb.setCharAt(index, letter.charAt(0));
                    index = password.indexOf(letter, index + 1);
                }
                emptyPassword = sb.toString();

                if (!emptyPassword.contains("_")) {
                    System.out.println("Wygrałeś, uzupełniłeś hasło: " + password);
                    break;
                }

                System.out.println("Brawo, trafiłeś. Obecne słowo: " + emptyPassword);
                System.out.println("\r\nCzy chcesz spróbować odgadnąć pełne hasło? Wpisz: tak lub nie.");
                var guess = scanner.nextLine();
                String answer = validateAnswer(guess);

                if (answer.equalsIgnoreCase("tak")) {
                    System.out.println("Podaj hasło");

                    if (!scanner.nextLine().equals(password)) {
                        if (lastHangman()) {
                            break;
                        }
                        System.out.println("Pudło, gramy dalej.");
                        System.out.println(hangManList.get(counter));
                        counter++;

                    } else {
                        System.out.println("Brawo, podałeś prawidłowe hasło: " + password.toUpperCase());
                        break;
                    }
                }
            }
        }
    }

    private boolean lastHangman() {
        if (counter == hangManList.size() - 1) {
            System.out.println("\r\nPrzegrałeś!!!!!!!!!!!!!");
            System.out.println(hangManList.get(counter));
            return true;
        }
        return false;
    }

    private String validateAnswer(String guess) {
        var answer = guess;

        while (!answer.equalsIgnoreCase("tak") && !answer.equalsIgnoreCase("nie")) {
            System.out.println("Wpisz tak lub nie");
            answer = scanner.nextLine();
        }
        return answer;
    }
}
